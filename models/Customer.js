const mongoose = require('mongoose');
const timestamp = require('mongoose-timestamp');

const CustomerSchema = new mongoose.Schema({
    mutasi:{
        type: Object,
        required:true,
        trim: true
    }
});

CustomerSchema.plugin(timestamp);
const Customer = mongoose.model('Customer',CustomerSchema);
module.exports = Customer;