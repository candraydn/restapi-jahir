const errors = require("restify-errors");
const Customer = require("../models/Customer");

module.exports = server => {
    //get all customer
    server.get('/customers', async (req, res, next)=>{
        try{
        const customers = await Customer.find({})
        res.send(customers);
        next();
        }catch(err){
            return next(new errors.InvalidContentError(err));
        }
    });

     //get spesifik customer
     server.get('/customers/:id', async (req, res, next)=>{
        try{
        const customers = await Customer.findById(req.params.id)
        res.send(customers);
        next();
        }catch(err){
            return next(new errors.ResourceNotFoundError(`Error! tidak ditemukan id ${req.params.id}`));
        }
    });

    //add customer
    server.post('/customers', async (req,res,next) =>{
        //cek json
        if(!req.is("application/json")){
            return next(new errors.InvalidContentError("Data tidak valid"));
        }
        const {mutasi} = req.body;
        const customers = new Customer({
            mutasi
        })

        try{
            const newCustomer = await customers.save();
            res.send(newCustomer);
        }catch(err){
            return next(new errors.InternalError(err.message))
        }
    })

    //update customer
    server.put('/customers/:id', async (req,res,next) =>{
        //cek json
        if(!req.is("application/json")){
            return next(new errors.InvalidContentError("Data tidak valid"));
        }
        try{
            const upCustomer = await Customer.findOneAndUpdate(
                {_id: req.params.id}, 
                req.body
            );
            res.send(200);
            next();
        }catch(err){
            return next(new errors.ResourceNotFoundError(`Error! tidak ditemukan id ${req.params.id}`));
        }
    })

    //delete cutomer
    server.del('/customers/:id', async (req, res, next)=>{
        try{
            const delCustomer = await Customer.findOneAndDelete({_id: req.params.id})
            res.send(204);
            next();
        }catch(err){
            next(new error.InternalError(err.message));
        }
    })
}