const restify = require('restify');
const mongoose = require('mongoose');
const config = require("./config");
mongoose.set('useFindAndModify', false);

const server = restify.createServer();

server.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin", "*");

    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    console.info(`${req.method} - ${req.url}`); 
    next();
});

//middleware
server.use(restify.plugins.bodyParser());

server.listen(config.PORT, () =>{
    mongoose.connect(config.MONGODB_URI, 
        { useNewUrlParser: true})
})

const db = mongoose.connection;

db.on('error', (err) => console.log(err));

db.once('open', () =>{
    require("./routes/customers")(server);
    console.log(`Server started on port ${config.PORT}`);
})